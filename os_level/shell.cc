#include "shell.hh"
#include "sys/types.h"
#include "sys/wait.h"

int main(){
    std::string input;
    std::string prompt;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  int promptFile = syscall(SYS_open, "config.txt", O_RDONLY, 0755);
  char byte[1];
  while(syscall(SYS_read, promptFile, byte, 1))
      std::cout << byte;

  syscall(SYS_close, promptFile);

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seekTest") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file(){
    #define MAX_FILENAME_LENGTH 100
    #define MAX_TEXT_LENGTH 1000

    char filename[MAX_FILENAME_LENGTH];
    char text[MAX_TEXT_LENGTH];

    printf("Geef de bestandsnaam: ");
    scanf("%s", filename);

    printf("Geef de tekst (eindig met <EOF>):\n");
    scanf(" %[^\n]s", text);

    int fd = syscall(SYS_creat,filename, 0644);

    if (syscall(SYS_write,fd, text, strlen(text)) < 0) {
        perror("Fout bij het schrijven naar het bestand");
        return;
    }
    syscall(SYS_close,fd);
    printf("Bestand '%s' succesvol aangemaakt.\n", filename);
}

void list() // ToDo: Implementeer volgens specificatie.
{
    pid_t child_pid = syscall(SYS_fork);

    if (child_pid < 0)
    {
        // Fork failed
        perror("Fork failed");
    }
    else if (child_pid == 0)
    {
        // Child process
        execl("/bin/ls", "ls", "-la", nullptr);
        perror("Exec failed");
        exit(1);
    }
    else
    {
        // Parent process
        syscall(SYS_wait4, -1, nullptr, 0, nullptr);
    }
}

void find() // ToDo: Implementeer volgens specificatie.
{
    char input[100];

    printf("Geef een zoekwoord: ");
    scanf("%s", input);

    int pipefd[2];
    syscall(SYS_pipe, pipefd);

    pid_t find_id;
    pid_t grep_id;

    find_id = syscall(SYS_fork);

    if (find_id == 0){ //Kinderprcess: Find
        syscall(SYS_close, pipefd[0]);
        syscall(SYS_dup2, pipefd[1], STDOUT_FILENO);
        syscall(SYS_close, pipefd[1]);
        execlp("find", "find", ".", NULL);
    }else{
        grep_id = syscall(SYS_fork);
        if (grep_id == 0){ //Kinderprocess: Grep
            syscall(SYS_close, pipefd[1]);
            syscall(SYS_dup2, pipefd[0], STDIN_FILENO);
            syscall(SYS_close, pipefd[0]);
            execlp("grep", "grep", input, NULL);
        }else{ //Parent Process
            syscall(SYS_close, pipefd[0]);
            syscall(SYS_close, pipefd[1]);
            //Wacht op child process
            syscall(SYS_wait4, NULL);
            syscall(SYS_wait4, NULL);
        }
    }
}

void seek() // ToDo: Implementeer volgens specificatie.
{
    const char* seek_testfile = "seek.txt";
    const char* loop_testfile = "loop.txt";
    const off_t empty_space = 5 * 1024 * 1024; //5 megabyte

    int seek_bestand = syscall(SYS_open, seek_testfile,  O_CREAT | O_WRONLY, 0644);
    int loop_bestand = syscall(SYS_open, loop_testfile,  O_CREAT | O_WRONLY, 0644);
    syscall(SYS_write, seek_bestand, "X", 1);
    syscall(SYS_write, loop_bestand, "X", 1);

    off_t positie = syscall(SYS_lseek, seek_bestand, 0, SEEK_END);
    char null_byte = '\0';

    for(off_t i = 0; i < empty_space; i++){
        syscall(SYS_write, loop_bestand, null_byte, 1);
    }

    syscall(SYS_lseek, seek_bestand, empty_space, SEEK_CUR);
    syscall(SYS_write, seek_bestand, null_byte, 1);

    syscall(SYS_write, seek_bestand, "X", 1);
    syscall(SYS_close, seek_bestand);
    syscall(SYS_write, loop_bestand, "X", 1);
    syscall(SYS_close, loop_bestand);

    std::cout << "Bestanden " << seek_testfile << "&" << loop_bestand  << " bijgewerkt" << std::endl;
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                                 // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                        //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
