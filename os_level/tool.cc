#include <iostream>
#include <string>

std::string translate(std::string line, int shift) {
    for (char& c : line) {
        // Alleen letters van het alfabet worden gecodeerd, behoud andere tekens ongewijzigd
        if (std::isalpha(c)) {
            // Bepaal het bereik van het alfabet (a-z of A-Z) op basis van het teken
            char start = std::islower(c) ? 'a' : 'A';
            char end = std::islower(c) ? 'z' : 'Z';

            // Voer de verschuiving uit met behulp van het Caesar-cijferalgoritme
            c = start + (c - start + shift) % (end - start + 1);
        }
    }
    return line;
}

int main(int argc, char* argv[]) {
    std::string line;
    int shift = 0;

    if (argc != 2) {
        std::cerr << "Deze functie heeft exact 1 argument nodig: de verschuiving" << std::endl;
        return -1;
    }

    try {
        shift = std::stoi(argv[1]);
    } catch (const std::exception& e) {
        std::cerr << "Ongeldig argument: " << e.what() << std::endl;
        return -1;
    }

    while (std::getline(std::cin, line)) {
        std::cout << translate(line, shift) << std::endl;
    }

    return 0;
}
